package nl.kritiekbeer.kingdoms;

import nl.kritiekbeer.kingdoms.commands.gates.GatesCommand;
import nl.kritiekbeer.kingdoms.commands.kingdoms.KingdomCommand;
import nl.kritiekbeer.kingdoms.defendthecastle.game.GameManager;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.kingdoms.gates.GateManager;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.kingdoms.kingdom.KingdomAllyInvite;
import nl.kritiekbeer.kingdoms.kingdom.KingdomInvite;
import nl.kritiekbeer.kingdoms.kingdom.KingdomManager;
import nl.kritiekbeer.kingdoms.listeners.*;
import nl.kritiekbeer.kingdoms.scoreboards.Boards;
import nl.kritiekbeer.servercore.api.CoreAPI;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Core extends JavaPlugin {

    private static Core main;
    public static CoreAPI api;

    public static KingdomManager kingdomManager;
    public static GameManager gameManager;
    public static GateManager gateManager;
    public static Boards boards;

    public static Material gateTool;
    public static List<Material> togglers;
    public static List<Material> materials;

    public static File configFile;
    public static File kingdomsFile;
    public static File gatesFile;
    public static YamlConfiguration config;
    public static YamlConfiguration kingdoms;
    public static YamlConfiguration gates;

    public static ItemStack claimToken;

    public static List<KingdomInvite> invites;
    public static List<KingdomAllyInvite> allyInvites;

    public static Core getInstance() {
        return main;
    }

    public void onEnable() {
        main = this;
        api = new CoreAPI();

        togglers = Arrays.asList(Material.STONE_BUTTON, Material.BIRCH_BUTTON, Material.ACACIA_BUTTON ,Material.BIRCH_BUTTON
                ,Material.DARK_OAK_BUTTON ,Material.JUNGLE_BUTTON ,Material.OAK_BUTTON
                ,Material.SPRUCE_BUTTON ,Material.LEVER);
        materials = Arrays.asList(Material.OAK_FENCE, Material.SPRUCE_FENCE, Material.JUNGLE_FENCE, Material.BIRCH_FENCE, Material.DARK_OAK_FENCE,
                Material.NETHER_BRICK_FENCE, Material.ACACIA_FENCE, Material.IRON_BARS, Material.COBBLESTONE_WALL, Material.MOSSY_COBBLESTONE_WALL,
                Material.GLASS_PANE, Material.WHITE_STAINED_GLASS_PANE, Material.ORANGE_STAINED_GLASS_PANE, Material.RED_STAINED_GLASS_PANE,
                Material.LIGHT_BLUE_STAINED_GLASS_PANE, Material.YELLOW_STAINED_GLASS_PANE, Material.LIME_STAINED_GLASS_PANE, Material.PINK_STAINED_GLASS_PANE,
                Material.BLACK_STAINED_GLASS_PANE, Material.BROWN_STAINED_GLASS_PANE, Material.CYAN_STAINED_GLASS_PANE, Material.PURPLE_STAINED_GLASS_PANE,
                Material.BLUE_STAINED_GLASS_PANE, Material.GREEN_STAINED_GLASS_PANE, Material.GRAY_STAINED_GLASS_PANE, Material.MAGENTA_STAINED_GLASS_PANE,
                Material.LIGHT_GRAY_STAINED_GLASS_PANE);
        gateTool = Material.GOLDEN_SHOVEL;

        startup();
    }

    public void onDisable() {
        kingdomManager.save();
        gateManager.save();
        save(kingdoms, kingdomsFile);
        save(gates, gatesFile);
    }

    private void startup() {
        setupFiles();
        load(config, configFile);
        load(kingdoms, kingdomsFile);
        load(gates, gatesFile);
        registerEvents();
        registerPermissions();
        registerCommands();

        kingdomManager = new KingdomManager();
        gameManager = new GameManager();
        gateManager = new GateManager();
        boards = new Boards();
        invites = new ArrayList<>();
        claimToken = new ItemStack(Material.EMERALD);

        run();
    }

    private void setupFiles() {
        configFile = new File(getDataFolder(), "config.yml");
        kingdomsFile = new File(getDataFolder(), "kingdoms.yml");
        gatesFile = new File(getDataFolder(), "gates.yml");
        firstRun();
        config = new YamlConfiguration();
        kingdoms = new YamlConfiguration();
        gates = new YamlConfiguration();
    }

    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new KingdomBlockBreak(), this);
        pm.registerEvents(new KingdomBlockPlace(), this);
        pm.registerEvents(new KingdomInteract(), this);
        pm.registerEvents(new GoldBlockBreak(), this);
        pm.registerEvents(new GoldBlockPlace(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new GateBlockBreak(), this);
        pm.registerEvents(new GateInteract(), this);
        pm.registerEvents(new GateToggle(), this);
        pm.registerEvents(new PlayerChat(), this);
        pm.registerEvents(new PlayerDeath(), this);
    }

    private void registerPermissions() {
        List<String> perms = Arrays.asList("kingdoms.break.all", "kingdoms.place.all");

        PluginManager pm = getServer().getPluginManager();
        for (String s : perms) {
            pm.addPermission(new Permission(s));
        }
    }

    private void registerCommands() {
        getCommand("kingdoms").setExecutor(new KingdomCommand());
        getCommand("gates").setExecutor(new GatesCommand());
    }

    public void firstRun() {
        try {
            if (!configFile.exists()) {
                configFile.getParentFile().mkdirs();
                copy(getResource("config.yml"), configFile);
            }
            if (!kingdomsFile.exists()) {
                kingdomsFile.getParentFile().mkdirs();
                copy(getResource("kingdoms.yml"), kingdomsFile);
            }
            if(!gatesFile.exists()) {
                gatesFile.getParentFile().mkdirs();
                copy(getResource("gates.yml"), gatesFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save(YamlConfiguration yaml, File file) {
        try {
            yaml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(YamlConfiguration yaml, File file) {
        try {
            yaml.load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            invites.forEach(KingdomInvite::count);
            gameManager.run();
            for (Player player : Bukkit.getOnlinePlayers()) {
                CPlayer cplayer = api.getPlayer(player.getUniqueId());
                boards.updateBoard(cplayer, BoardType.valueOf((String)cplayer.getVariable("boardtype")));
            }
        }, 20, 20);
    }

    public List<Kingdom> getInvites(CPlayer player) {
        List<Kingdom> list = new ArrayList<>();
        for (KingdomInvite invite : invites) {
            if (invite.getTarget().equals(player))
                list.add(invite.getKingdom());
        }
        return list;
    }

    public void removeInvite(CPlayer player, Kingdom kingdom) {
        for (KingdomInvite invite : invites) {
            if (invite.getTarget().equals(player) && invite.getKingdom().equals(kingdom)) {
                invites.remove(invite);
                break;
            }
        }
    }

    public List<Kingdom> getAllyInvites(Kingdom kingdom) {
        List<Kingdom> list = new ArrayList<>();
        for (KingdomAllyInvite invite : allyInvites) {
            if (invite.getTarget().equals(kingdom))
                list.add(invite.getKingdom());
        }
        return list;
    }

    public void removeInvite(Kingdom invited, Kingdom inviter) {
        for (KingdomAllyInvite invite : allyInvites) {
            if (invite.getTarget().equals(invited) && invite.getKingdom().equals(inviter)) {
                invites.remove(invite);
                break;
            }
        }
    }
}

