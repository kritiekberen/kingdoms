package nl.kritiekbeer.kingdoms.scoreboards;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.player.CPlayer;

public abstract class Board {

    public void setBoard(CPlayer player, BoardType type) {
        if (type.equals(BoardType.none)) player.getPlayer().getPlayer().setScoreboard(null);
        getBoard(type).setScoreboard(player);
    }

    public void updateBoard(CPlayer player, BoardType type) {
        if (type.equals(BoardType.none)) player.getPlayer().getPlayer().setScoreboard(null);
        getBoard(type).updateScoreboard(player);
    }

    public Board getBoard(BoardType type) {
        for (Board board : Core.boards.list) {
            if (board.getType().equals(type))
                return board;
        }
        return null;
    }

    public abstract BoardType getType();
    public abstract void setScoreboard(CPlayer player);
    public abstract void updateScoreboard(CPlayer player);
}
