package nl.kritiekbeer.kingdoms.scoreboards;

import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public class Boards {

    public List<Board> list = new ArrayList<>();

    public Boards() {
        list.add(new KingdomScreen());
        list.add(new StatsScreen());
        list.add(new MapScreen());
    }

    public void setBoard(CPlayer player, BoardType type) {
        if (type.equals(BoardType.none)) {
            player.getPlayer().getPlayer().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            return;
        }
        getBoard(type).setScoreboard(player);
    }

    public void updateBoard(CPlayer player, BoardType type) {
        if (type.equals(BoardType.none)) {
            player.getPlayer().getPlayer().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            return;
        }
        getBoard(type).updateScoreboard(player);
    }

    public Board getBoard(BoardType type) {
        for (Board board : list) {
            if (board.getType().equals(type))
                return board;
        }
        return null;
    }
}
