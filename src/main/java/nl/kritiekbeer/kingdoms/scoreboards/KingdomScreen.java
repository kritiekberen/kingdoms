package nl.kritiekbeer.kingdoms.scoreboards;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class KingdomScreen extends Board {

    public BoardType getType() {
        return BoardType.kingdom;
    }

    public void setScoreboard(CPlayer player) {
        String kingdom = Core.kingdomManager.getKingdomForPlayer(player) == null ? "none" : Core.kingdomManager.getKingdomForPlayer(player).getName();
        String rank = Core.kingdomManager.getKingdomForPlayer(player) == null ? "none" : Core.kingdomManager.getKingdomForPlayer(player).getRank(player).name().toLowerCase();
        Player p = player.getPlayer().getPlayer();

        Scoreboard board  = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("KingdomsBoard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("Kingdoms");

        Score blank1 = objective.getScore("" + ChatColor.RED);
        Score blank2 = objective.getScore("" + ChatColor.YELLOW );
        Score blank3 = objective.getScore("" + ChatColor.GREEN);
        blank1.setScore(8);
        blank2.setScore(5);
        blank3.setScore(2);

        Score currentKingdom = objective.getScore(ChatColor.GRAY + "Your kingdom:");
        Score currentRank = objective.getScore(ChatColor.GRAY + "Your rank:");
        Score totalKingdoms = objective.getScore(ChatColor.GRAY + "Total kingdoms:");
        currentKingdom.setScore(7);
        currentRank.setScore(4);
        totalKingdoms.setScore(1);

        Team kingdomDisplay = board.registerNewTeam("kingdomDisplay");
        kingdomDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.AQUA);
        kingdomDisplay.setPrefix("" + ChatColor.AQUA + kingdom);
        objective.getScore(ChatColor.AQUA + "" + ChatColor.AQUA).setScore(6);

        Team rankDisplay = board.registerNewTeam("rankDisplay");
        rankDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.YELLOW);
        rankDisplay.setPrefix("" + ChatColor.AQUA + rank);
        objective.getScore(ChatColor.AQUA + "" + ChatColor.YELLOW).setScore(3);

        Team totalDisplay = board.registerNewTeam("totalDisplay");
        totalDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.RED);
        totalDisplay.setPrefix("" + ChatColor.AQUA + Core.kingdomManager.getKingdoms().size());
        objective.getScore(ChatColor.AQUA + "" + ChatColor.RED).setScore(0);

        p.setScoreboard(board);
    }

    public void updateScoreboard(CPlayer player) {
        String kingdom = Core.kingdomManager.getKingdomForPlayer(player) == null ? "none" : Core.kingdomManager.getKingdomForPlayer(player).getName();
        String rank = Core.kingdomManager.getKingdomForPlayer(player) == null ? "none" : Core.kingdomManager.getKingdomForPlayer(player).getRank(player).name().toLowerCase();
        Player p = player.getPlayer().getPlayer();

        Scoreboard board = player.getPlayer().getPlayer().getScoreboard();

        board.getTeam("kingdomDisplay").setPrefix("" + ChatColor.AQUA + kingdom);
        board.getTeam("rankDisplay").setPrefix("" + ChatColor.AQUA + rank);
        board.getTeam("totalDisplay").setPrefix("" + ChatColor.AQUA + Core.kingdomManager.getKingdoms().size());

        p.setScoreboard(board);
    }
}
