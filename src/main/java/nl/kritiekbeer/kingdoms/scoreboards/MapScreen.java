package nl.kritiekbeer.kingdoms.scoreboards;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class MapScreen extends Board {

    public BoardType getType() {
        return BoardType.map;
    }

    public void setScoreboard(CPlayer player) {
        Player p = player.getPlayer().getPlayer();
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        String[] map = new String[9];

        int countx = 0;

        for (int x = p.getLocation().getChunk().getX()-4; x < p.getLocation().getChunk().getX() + 5; x++) {
            map[countx] = "";
            for (int z = p.getLocation().getChunk().getZ()-4; z < p.getLocation().getChunk().getZ() + 5; z++) {
                String coord = ChatColor.WHITE + "/";
                if (Core.kingdomManager.isClaimed(p.getWorld().getChunkAt(x,z))) {
                    Kingdom claimed = Core.kingdomManager.getKingdom(p.getWorld().getChunkAt(x,z));
                    if (claimed.equals(kingdom)) {
                        coord = ChatColor.GOLD + "X";
                    } else if (kingdom.getAllies().contains(claimed)) {
                        coord = ChatColor.GREEN + "X";
                    } else {
                        coord = ChatColor.WHITE + "X";
                    }
                }

                map[countx] += coord;
            }
            countx++;
        }

        Scoreboard board  = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("KingdomMapBoard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("Kingdom Map");

        Score blank1 = objective.getScore("" + ChatColor.RED);
        blank1.setScore(9);

        Team line1 = board.registerNewTeam("line1");
        line1.addEntry(ChatColor.AQUA + "" + ChatColor.AQUA);
        line1.setPrefix(map[0].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.AQUA).setScore(8);

        Team line2 = board.registerNewTeam("line2");
        line2.addEntry(ChatColor.AQUA + "" + ChatColor.GREEN);
        line2.setPrefix(map[1].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.GREEN).setScore(7);

        Team line3 = board.registerNewTeam("line3");
        line3.addEntry(ChatColor.AQUA + "" + ChatColor.YELLOW);
        line3.setPrefix(map[2].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.YELLOW).setScore(6);

        Team line4 = board.registerNewTeam("line4");
        line4.addEntry(ChatColor.AQUA + "" + ChatColor.RED);
        line4.setPrefix(map[3].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.RED).setScore(5);

        Team line5 = board.registerNewTeam("line5");
        line5.addEntry(ChatColor.AQUA + "" + ChatColor.GOLD);
        line5.setPrefix(map[4].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.GOLD).setScore(4);

        Team line6 = board.registerNewTeam("line6");
        line6.addEntry(ChatColor.AQUA + "" + ChatColor.BLUE);
        line6.setPrefix(map[5].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.BLUE).setScore(3);

        Team line7 = board.registerNewTeam("line7");
        line7.addEntry(ChatColor.AQUA + "" + ChatColor.DARK_BLUE);
        line7.setPrefix(map[6].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.DARK_BLUE).setScore(2);

        Team line8 = board.registerNewTeam("line8");
        line8.addEntry(ChatColor.AQUA + "" + ChatColor.DARK_AQUA);
        line8.setPrefix(map[7].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.DARK_AQUA).setScore(1);

        Team line9 = board.registerNewTeam("line9");
        line9.addEntry(ChatColor.AQUA + "" + ChatColor.GRAY);
        line9.setPrefix(map[8].replace("null", ""));
        objective.getScore(ChatColor.AQUA + "" + ChatColor.GRAY).setScore(0);


        p.setScoreboard(board);
    }

    public void updateScoreboard(CPlayer player) {
        Player p = player.getPlayer().getPlayer();
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        String[] map = new String[9];

        int countx = 0;

        for (int x = p.getLocation().getChunk().getX()-4; x < p.getLocation().getChunk().getX() + 5; x++) {
            for (int z = p.getLocation().getChunk().getZ()-4; z < p.getLocation().getChunk().getZ() + 5; z++) {
                String coord = ChatColor.WHITE + "/";
                if (Core.kingdomManager.isClaimed(p.getWorld().getChunkAt(x,z))) {
                    Chunk chunk = p.getWorld().getChunkAt(x, z);

                    if (kingdom == null && Core.kingdomManager.isClaimed(chunk)) {
                        coord = "X";
                    }

                    Kingdom claimed = Core.kingdomManager.getKingdom(chunk);
                    if (claimed != null) {
                        if (claimed.equals(kingdom)) {
                            coord = ChatColor.GOLD + "X";
                        } else if (kingdom.getAllies().contains(claimed)) {
                            coord = ChatColor.GREEN + "X";
                        } else {
                            coord = ChatColor.WHITE + "X";
                        }
                    }
                }

                map[countx] += coord;
            }
            countx++;
        }

        Scoreboard board = player.getPlayer().getPlayer().getScoreboard();

        board.getTeam("line1").setPrefix(map[0].replace("null", ""));
        board.getTeam("line2").setPrefix(map[1].replace("null", ""));
        board.getTeam("line3").setPrefix(map[2].replace("null", ""));
        board.getTeam("line4").setPrefix(map[3].replace("null", ""));
        board.getTeam("line5").setPrefix(map[4].replace("null", ""));
        board.getTeam("line6").setPrefix(map[5].replace("null", ""));
        board.getTeam("line7").setPrefix(map[6].replace("null", ""));
        board.getTeam("line8").setPrefix(map[7].replace("null", ""));
        board.getTeam("line9").setPrefix(map[8].replace("null", ""));

        p.setScoreboard(board);
    }
}
