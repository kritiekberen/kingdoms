package nl.kritiekbeer.kingdoms.scoreboards;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class StatsScreen extends Board {

    public BoardType getType() {
        return BoardType.stats;
    }

    public void setScoreboard(CPlayer player) {
        Player p = player.getPlayer().getPlayer();
        int kills = player.getVariable("kills") == null ? 0 : (int)player.getVariable("kills");
        int deaths = player.getVariable("deaths") == null ? 0 : (int)player.getVariable("deaths");

        Scoreboard board  = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("StatsBoard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("Stats");

        Score blank1 = objective.getScore("" + ChatColor.RED);
        Score blank2 = objective.getScore("" + ChatColor.YELLOW );
        Score blank3 = objective.getScore("" + ChatColor.GREEN);
        blank1.setScore(8);
        blank2.setScore(5);
        blank3.setScore(2);

        Score currentPlaytime = objective.getScore(ChatColor.GRAY + "Your playtime:");
        Score currentKills = objective.getScore(ChatColor.GRAY + "Your kills:");
        Score currentDeaths = objective.getScore(ChatColor.GRAY + "Your deaths:");
        currentPlaytime.setScore(7);
        currentKills.setScore(4);
        currentDeaths.setScore(1);

        Team kingdomDisplay = board.registerNewTeam("playtimeDisplay");
        kingdomDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.AQUA);
        kingdomDisplay.setPrefix("" + ChatColor.AQUA + player.getPlayTime() + " minutes");
        objective.getScore(ChatColor.AQUA + "" + ChatColor.AQUA).setScore(6);

        Team rankDisplay = board.registerNewTeam("killsDisplay");
        rankDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.YELLOW);
        rankDisplay.setPrefix("" + ChatColor.AQUA + kills + " kills");
        objective.getScore(ChatColor.AQUA + "" + ChatColor.YELLOW).setScore(3);

        Team totalDisplay = board.registerNewTeam("deathsDisplay");
        totalDisplay.addEntry(ChatColor.AQUA + "" + ChatColor.RED);
        totalDisplay.setPrefix("" + ChatColor.AQUA + deaths + " deaths");
        objective.getScore(ChatColor.AQUA + "" + ChatColor.RED).setScore(0);

        p.setScoreboard(board);
    }

    public void updateScoreboard(CPlayer player) {
        Player p = player.getPlayer().getPlayer();

        int kills = player.getVariable("kills") == null ? 0 : (int)player.getVariable("kills");
        int deaths = player.getVariable("deaths") == null ? 0 : (int)player.getVariable("deaths");

        Scoreboard board = player.getPlayer().getPlayer().getScoreboard();

        board.getTeam("playtimeDisplay").setPrefix("" + ChatColor.AQUA + player.getPlayTime() + " minutes");
        board.getTeam("killsDisplay").setPrefix("" + ChatColor.AQUA + kills + " kills");
        board.getTeam("deathsDisplay").setPrefix("" + ChatColor.AQUA + deaths + " deaths");

        p.setScoreboard(board);
    }
}
