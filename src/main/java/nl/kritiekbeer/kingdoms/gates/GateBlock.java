package nl.kritiekbeer.kingdoms.gates;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class GateBlock {

    private Block block;
    private int id;

    public GateBlock(int id, Location location) {
        this.id = id;
        this.block = location.getBlock();
    }

    public void updateBlockType(Material material) {
        this.block.setType(material);
    }

    public int getID() {
        return this.id;
    }

    public Location getLocation() {
        return this.block.getLocation();
    }
}
