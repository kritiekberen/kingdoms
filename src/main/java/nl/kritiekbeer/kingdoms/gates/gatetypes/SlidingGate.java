package nl.kritiekbeer.kingdoms.gates.gatetypes;

import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.gates.GateRow;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;

import java.util.List;

public class SlidingGate extends Gate {

    public SlidingGate(Kingdom owner, String name, Location leftBound, Location rightBound, Material material) {
        super(owner, GateType.SLIDING, name, leftBound, rightBound, material);
    }

    public SlidingGate(Kingdom owner, String name, int id, List<GateRow> rows, Material material, List<Location> togglers, Boolean closed, World world, Sound openSound, Sound closeSound, int openInterval, int closeInterval) {
        super(owner,GateType.DRAWBRIDGE,name,id,rows,material,togglers,closed, world, openSound, closeSound, openInterval, closeInterval);
    }

    @Override
    public void construct() {
        int leftY = leftBound.getBlockY();
        int rightY = rightBound.getBlockY();
        int height, topY;

        if (leftY > rightY) {
            height = leftY - rightY;
            topY = leftY;
        } else {
            height = rightY - leftY;
            topY = rightY;
        }

        int difference = height;
        for (int y = topY; difference > -1; y--) {
            Location left = new Location(leftBound.getWorld(), leftBound.getBlockX(), y, leftBound.getBlockZ());
            Location right = new Location(rightBound.getWorld(), rightBound.getBlockX(), y, rightBound.getBlockZ());
            rows.add(new GateRow(gateID, difference, left, right, material, getGateType(), false));
            difference--;
        }
    }
}
