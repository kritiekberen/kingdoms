package nl.kritiekbeer.kingdoms.gates.gatetypes;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.gates.GateRow;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class FenceGate extends Gate {

    public FenceGate(Kingdom owner, String name, Location leftBound, Location rightBound, Material material) {
        super(owner, GateType.FENCE, name, leftBound, rightBound, material);
    }

    public FenceGate(Kingdom owner, String name, int id, List<GateRow> rows, Material material, List<Location> togglers, Boolean closed, World world, Sound openSound, Sound closeSound, int openInterval, int closeInterval) {
        super(owner,GateType.DRAWBRIDGE,name,id,rows,material,togglers,closed, world, openSound, closeSound, openInterval, closeInterval);
    }

    @Override
    public void construct() {
        int leftX = this.leftBound.getBlockX();
        int leftZ = this.leftBound.getBlockZ();
        int rightX = this.rightBound.getBlockX();
        int rightZ = this.rightBound.getBlockZ();

        int differenceX = leftX - rightX;
        int differenceZ = leftZ - rightZ;
        if (differenceX < 0) differenceX *= -1;
        if (differenceZ < 0) differenceZ *= -1;

        boolean isX = false;

        if (differenceX > differenceZ) {
            isX = true;
        }

        int low = leftX;
        int max = rightX;
        if (!isX) {
            low = leftZ;
            max = rightZ;
        }

        int id = 0;
        for (int i = low; i < max+1; i++) {
            Location left, right;
            if (isX) {
                left = new Location(leftBound.getWorld(), i, leftBound.getY(), leftBound.getZ());
                right = new Location(rightBound.getWorld(), i, rightBound.getY(), leftBound.getZ());
            } else {
                left = new Location(leftBound.getWorld(), leftBound.getX(), leftBound.getY(), i);
                right = new Location(rightBound.getWorld(), leftBound.getX(), rightBound.getY(), i);
            }

            GateRow row = new GateRow(gateID, id, left, right, material, getGateType(), true);
            rows.add(row);
            id++;
        }
        for (int i = low; i > max-1; i--) {
            Location left, right;
            if (isX) {
                left = new Location(leftBound.getWorld(), i, leftBound.getY(), leftBound.getZ());
                right = new Location(rightBound.getWorld(), i, rightBound.getY(), leftBound.getZ());
            } else {
                left = new Location(leftBound.getWorld(), leftBound.getX(), leftBound.getY(), i);
                right = new Location(rightBound.getWorld(), leftBound.getX(), rightBound.getY(), i);
            }

            GateRow row = new GateRow(gateID, id, left, right, material, getGateType(), true);
            rows.add(row);
            id++;
        }
    }

    @Override
    public void close() {
        closed = true;

        new BukkitRunnable() {
            int index = 0;
            public void run() {
                rows.get(index).close();
                index++;
                if (index == rows.size()) this.cancel();
            }
        }.runTaskTimer(Core.getInstance(), 0, 10);

        rows.get(rows.size()-1).close();
        rows.get(0).close();
    }
}
