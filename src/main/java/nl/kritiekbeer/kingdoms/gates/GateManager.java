package nl.kritiekbeer.kingdoms.gates;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.gates.gatetypes.BridgeGate;
import nl.kritiekbeer.kingdoms.gates.gatetypes.FenceGate;
import nl.kritiekbeer.kingdoms.gates.gatetypes.SlidingGate;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.utils.LocationUtils;
import org.bukkit.*;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class GateManager {

    private List<Gate> gates;

    public GateManager() {
        this.gates = new ArrayList<>();
        load();
    }

    public Gate createGate(Kingdom owner, GateType gateType, String name, Location leftBound, Location rightBound, Material material) {
        Gate gate = null;

        if (gateType == GateType.FENCE) gate = new FenceGate(owner, name, leftBound, rightBound, material);
        if (gateType == GateType.DRAWBRIDGE) gate = new BridgeGate(owner, name, leftBound, rightBound, material);
        if (gateType == GateType.SLIDING) gate = new SlidingGate(owner, name, leftBound, rightBound, material);

        if (gate != null) {
            this.gates.add(gate);
            return gate;
        }
        return null;
    }

    public void removeGate(Gate gate) {
        if (this.gates.contains(gate)) {
            Core.gates.set(gate.getOwner().getID() + "." + gate.getID(), null);
            this.gates.remove(gate);
        }
    }

    public Gate getGate(int id) {
        for (Gate gate : gates) {
            if (gate.getID() == id)
                return gate;
        } return null;
    }

    /**
     * Get a gate by the toggle location
     *
     * @param location Location of toggle
     * @return found Gate or null if not found
     */
    public List<Gate> getGatesByToggle(Location location) {
        List<Gate> gateList = new ArrayList<>();
        for (Gate gate : gates) {
            if (gate.getTogglers().contains(location)) {
                gateList.add(gate);
            }
        } return gateList;
    }

    public void save() {
        for (Gate gate : gates) {
            String uuid = gate.getOwner().getID() + "";
            Core.gates.set(uuid + "." + gate.getID() + ".material", gate.getMaterial().name());
            Core.gates.set(uuid + "." + gate.getID() + ".closed", gate.isClosed());
            Core.gates.set(uuid + "." + gate.getID() + ".name", gate.getName());
            Core.gates.set(uuid + "." + gate.getID() + ".gatetype", gate.getGateType().name().toUpperCase());
            Core.gates.set(uuid + "." + gate.getID() + ".world", gate.getWorld().getName());
            Core.gates.set(uuid + "." + gate.getID() + ".opensound", gate.getOpenSound().name());
            Core.gates.set(uuid + "." + gate.getID() + ".closesound", gate.getCloseSound().name());
            Core.gates.set(uuid + "." + gate.getID() + ".openinterval", gate.getOpenInterval());
            Core.gates.set(uuid + "." + gate.getID() + ".closeinterval", gate.getCloseInterval());
            for (int i = 0; i < gate.getTogglers().size(); i++) {
                Core.gates.set(uuid + "." + gate.getID() + ".togglers." + i, LocationUtils.locToString(gate.getTogglers().get(i)));
            }
            for (GateRow row : gate.getRows()) {
                for (GateBlock block : row.getBlocks()) {
                    String path = uuid + "." + gate.getID() + ".rows." + row.getID() + ".blocks." + block.getID();
                    Core.gates.set(path, LocationUtils.locToString(block.getLocation()));
                }
                for (int i = 0; i < row.getWalls().size(); i++) {
                    String path = uuid + "." + gate.getID() + ".rows." + row.getID() + ".walls." + i;
                    Core.gates.set(path, LocationUtils.locToString(row.getWalls().get(i).getLocation()));
                }
            }
        }
    }

    public void load() {
        for (String uuid : Core.gates.getConfigurationSection("").getKeys(false)) {
            Kingdom kingdom = Core.kingdomManager.getKingdom(Integer.valueOf(uuid));

            for (String gateId : Core.gates.getConfigurationSection(uuid).getKeys(false)) {
                Material material = Material.valueOf(Core.gates.getString(uuid + "." + gateId + ".material", Material.OAK_FENCE.name()));
                Boolean closed = Core.gates.getBoolean(uuid + "." + gateId + ".closed", false);
                String name = Core.gates.getString(uuid + "." + gateId + ".name");
                GateType gateType = GateType.valueOf(Core.gates.getString(uuid + "." + gateId + ".gatetype", GateType.SLIDING.name()).toUpperCase());
                World world = Bukkit.getWorld(Core.gates.getString(uuid + "." + gateId + ".world", "world"));
                List<Location> togglers = new ArrayList<>();
                List<GateRow> rows = new ArrayList<>();
                int openInterval = Core.gates.getInt(uuid + "." + gateId + ".openinterval", 10);
                int closeInterval = Core.gates.getInt(uuid + "." + gateId + ".closeinterval", 10);

                Sound openSound = Sound.valueOf(Core.gates.getString(uuid + "." + gateId + ".opensound", Sound.BLOCK_WOOD_BREAK.name()));
                Sound closeSound = Sound.valueOf(Core.gates.getString(uuid + "." + gateId + ".closesound", Sound.BLOCK_WOOD_PLACE.name()));

                if (Core.gates.isConfigurationSection(uuid + "." + gateId + ".togglers")) {
                    for (String toggler : Core.gates.getConfigurationSection(uuid + "." + gateId + ".togglers").getKeys(false)) {
                        togglers.add(LocationUtils.locFromString(Core.gates.getString(uuid + "." + gateId + ".togglers." + toggler)));
                    }
                }

                for (String rowId : Core.gates.getConfigurationSection(uuid + "." + gateId + ".rows").getKeys(false)) {
                    List<GateBlock> blocks = new ArrayList<>();
                    List<Block> walls = new ArrayList<>();
                    if (Core.gates.isConfigurationSection(uuid + "." + gateId + ".rows." + rowId + ".blocks")) {
                        for (String blockId : Core.gates.getConfigurationSection(uuid + "." + gateId + ".rows." + rowId + ".blocks").getKeys(false)) {
                            Location blockLocation = LocationUtils.locFromString(Core.gates.getString(uuid + "." + gateId + ".rows." + rowId + ".blocks." + blockId));
                            GateBlock gateBlock = new GateBlock(Integer.valueOf(blockId), blockLocation);
                            blocks.add(gateBlock);
                        }
                    }
                    if (Core.gates.isConfigurationSection(uuid + "." + gateId + ".rows." + rowId + ".walls")) {
                        for (String wallId : Core.gates.getConfigurationSection(uuid + "." + gateId + ".rows." + rowId + ".walls").getKeys(false)) {
                            Location wallLocation = LocationUtils.locFromString(Core.gates.getString(uuid + "." + gateId + ".rows." + rowId + ".walls." + wallId));
                            Block wall = world.getBlockAt(wallLocation);
                            walls.add(wall);
                        }
                    }
                    GateRow gateRow = new GateRow(Integer.valueOf(gateId), Integer.valueOf(rowId), blocks, walls, material);
                    rows.add(gateRow);
                }
                Gate gate = null;

                if (gateType == GateType.FENCE) gate = new FenceGate(kingdom, name, Integer.valueOf(gateId), rows, material, togglers, closed, world, openSound, closeSound, openInterval, closeInterval);
                if (gateType == GateType.DRAWBRIDGE) gate = new BridgeGate(kingdom, name, Integer.valueOf(gateId), rows, material, togglers, closed, world, openSound, closeSound, openInterval, closeInterval);
                if (gateType == GateType.SLIDING) gate = new SlidingGate(kingdom, name, Integer.valueOf(gateId), rows, material, togglers, closed, world, openSound, closeSound, openInterval, closeInterval);

                gates.add(gate);
            }
        }
    }

    public Gate getGate(Kingdom owner, String name) {
        for (Gate gate : getGates(owner)) {
            if (gate.getName().equalsIgnoreCase(name)) {
                return gate;
            }
        } return null;
    }

    public List<Gate> getGates() {
        return this.gates;
    }

    public List<Gate> getGates(Kingdom owner) {
        List<Gate> list = new ArrayList<>();
        for (Gate gate : gates) {
            if (gate.getOwner().equals(owner)) {
                list.add(gate);
            }
        } return list;
    }
}
