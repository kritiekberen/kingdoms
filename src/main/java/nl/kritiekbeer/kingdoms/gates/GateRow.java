package nl.kritiekbeer.kingdoms.gates;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.utils.IntegerUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class GateRow {

    private List<GateBlock> blocks;
    private List<Block> walls;
    private Material fenceType;
    private int id;
    private int gateId;

    public GateRow(int gateId, int id, List<GateBlock> blocks, List<Block> walls, Material material) {
        this.gateId = gateId;
        this.id = id;
        this.fenceType = material;
        this.blocks = blocks;
        this.walls = walls;
    }

    public GateRow(int gateId, int id, Location left, Location right, Material material, GateType gateType, boolean up) {
        this.gateId = gateId;
        this.id = id;
        this.fenceType = material;
        this.blocks = new ArrayList<>();
        this.walls = new ArrayList<>();

        int leftX = left.getBlockX();
        int leftZ = left.getBlockZ();
        int leftY = left.getBlockY();
        int rightX = right.getBlockX();
        int rightZ = right.getBlockZ();
        int rightY = right.getBlockY();

        int[] listX = IntegerUtils.getIntsBetween(leftX, rightX);
        int[] listZ = IntegerUtils.getIntsBetween(leftZ, rightZ);
        int[] listY = IntegerUtils.getIntsBetween(leftY, rightY);

        if (up) {
            for (int y = 0; y < listY.length; y++) {
                Location location = new Location(left.getWorld(), left.getBlockX(), listY[y], left.getBlockZ());

                if (location.getBlock().getType().equals(material) || location.getBlock().getType().equals(Material.AIR) || location.getBlock().getType().equals(Material.WATER)) {
                    blocks.add(new GateBlock(y, location));
                    continue;
                }
                walls.add(location.getBlock());
            }
        }
        else {
            if (listX.length > 0) {
                for (int x = 0; x < listX.length; x++) {
                    Location location = new Location(left.getWorld(), listX[x], left.getBlockY(), left.getBlockZ());
                    if (location.getBlock().getType().equals(material) || location.getBlock().getType().equals(Material.AIR) || location.getBlock().getType().equals(Material.WATER)) {
                        blocks.add(new GateBlock(x, location));
                        continue;
                    }
                    walls.add(location.getBlock());
                }
                walls.add(left.getWorld().getBlockAt((listX[0] - 1), left.getBlockY(), left.getBlockZ()));
                walls.add(left.getWorld().getBlockAt((listX[listX.length - 1] + 1), left.getBlockY(), left.getBlockZ()));
            }
            if (listZ.length > 0) {
                for (int z = 0; z < listZ.length; z++) {
                    Location location = new Location(left.getWorld(), right.getBlockX(), right.getBlockY(), listZ[z]);
                    if (location.getBlock().getType().equals(material) || location.getBlock().getType().equals(Material.AIR) || location.getBlock().getType().equals(Material.WATER)) {
                        blocks.add(new GateBlock(z, location));
                        continue;
                    }
                    walls.add(location.getBlock());
                }

                walls.add(left.getWorld().getBlockAt(left.getBlockX(), left.getBlockY(), (listZ[0] - 1)));
                walls.add(left.getWorld().getBlockAt(left.getBlockX(), left.getBlockY(), (listZ[listZ.length - 1] + 1)));
            }
        }
    }

    public void close() {
        if (blocks.size() > 0) {
            blocks.get(0).getLocation().getWorld().playSound(blocks.get(0).getLocation(), Core.gateManager.getGate(gateId).getCloseSound(), 1, 1);
            blocks.forEach(block -> block.updateBlockType(this.fenceType));
        }
        if (walls.size() > 0) {
            walls.forEach(wall -> {
                Material material = wall.getType();
                wall.setType(Material.STONE);
                wall.setType(material);
                wall.getState().update(true, true);
            });
        }
    }

    public void open() {
        if (blocks.size() > 0) {
            blocks.get(0).getLocation().getWorld().playSound(blocks.get(0).getLocation(), Core.gateManager.getGate(gateId).getOpenSound(), 1, 1);
            blocks.forEach(block -> block.updateBlockType(Material.AIR));
        }
    }

    public void setMaterial(Material material) {
        this.fenceType = material;
    }

    public int getID () {
        return this.id;
    }

    public List<GateBlock> getBlocks() {
        return this.blocks;
    }

    public List<Block> getWalls() {
        return this.walls;
    }
}
