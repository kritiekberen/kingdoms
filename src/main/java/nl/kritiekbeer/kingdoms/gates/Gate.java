package nl.kritiekbeer.kingdoms.gates;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public abstract class Gate {

    public List<GateRow> rows;
    public Material material;
    private List<Location> togglers;
    public Boolean closed = false;
    public int gateID, openInterval, closeInterval;
    private Kingdom owner;
    private String name;
    private GateType gateType;
    public Location leftBound, rightBound;
    public World world;
    private Boolean closing = false;
    private Sound openSound, closeSound;

    public Gate(Kingdom owner, GateType gateType, String name, Location leftBound, Location rightBound, Material material) {
        this.owner = owner;
        this.name = name;
        this.gateID = Core.gateManager.getGates().size();
        this.rows = new ArrayList<>();
        this.togglers = new ArrayList<>();
        this.gateType = gateType;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.material = material;
        this.world = leftBound.getWorld();

        this.openSound = Sound.BLOCK_WOOD_PLACE;
        this.closeSound = Sound.BLOCK_WOOD_BREAK;

        this.openInterval = 10;
        this.closeInterval = 10;

        this.construct();
    }

    public Gate(Kingdom owner, GateType gateType, String name, int id, List<GateRow> rows, Material material, List<Location> togglers, Boolean closed, World world, Sound openSound, Sound closeSound, int openInterval, int closeInterval) {
        this.owner = owner;
        this.gateType = gateType;
        this.name = name;
        this.gateID = id;
        this.rows = rows;
        this.material = material;
        this.togglers = togglers;
        this.closed = closed;
        this.world = world;
        this.openSound = openSound;
        this.closeSound = closeSound;
        this.openInterval = openInterval;
        this.closeInterval = closeInterval;
    }

    public abstract void construct();

    public void close() {
        this.closed = true;
        this.closing = true;

        new BukkitRunnable() {
            int index = 0;
            public void run() {
                rows.get(index).close();
                index++;
                if (index == rows.size()) {
                    closing = false;
                    this.cancel();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, getCloseInterval());
    }

    public void open() {
        this.closed = false;
        this.closing = true;

        new BukkitRunnable() {
            int index = rows.size()-1;
            public void run() {
                rows.get(index).open();
                index--;
                if (index == -1) {
                    closing = false;
                    this.cancel();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, getOpenInterval());
    }

    public void setMaterial(Material material) {
        this.material = material;
        for (GateRow row : rows) {
            row.setMaterial(material);
        }
    }

    public void addToggler(Location location) {
        if(!this.togglers.contains(location)) this.togglers.add(location);
    }

    public void delToggler(Location location) {
        if(this.togglers.contains(location)) this.togglers.remove(location);
    }

    public List<Location> getTogglers() {
        return this.togglers;
    }

    public Boolean isClosed() {
        return this.closed;
    }

    public int getID() {
        return this.gateID;
    }

    public List<GateRow> getRows() {
        return this.rows;
    }

    public Material getMaterial() {
        return this.material;
    }

    public Kingdom getOwner() {
        return this.owner;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllowed(CPlayer player) {
        Kingdom own = Core.kingdomManager.getKingdom(player);
        if (own == null) return false;

        if (owner.equals(own) || owner.getAllies().contains(own)) return true;
        return false;
    }

    public boolean isAllowedToEdit(CPlayer player) {
        Kingdom own = Core.kingdomManager.getKingdom(player);
        if (own == null) return false;

        if (own.getRank(player).equals(RankType.KING) || own.getRank(player).equals(RankType.DUKE)) return true;
        return false;
    }

    public GateType getGateType() {
        return this.gateType;
    }

    public World getWorld() {
        return this.world;
    }

    public Boolean isClosing() {
        return this.closing;
    }

    public boolean isGateBlock(Location location) {
        for (GateRow row : rows) {
            for (GateBlock block : row.getBlocks()) {
                if (block.getLocation().equals(location))
                    return true;
            }
        } return false;
    }

    public Sound getOpenSound() {
        return this.openSound;
    }

    public Sound getCloseSound() {
        return this.closeSound;
    }

    public void setOpenSound(Sound sound) {
        this.openSound = sound;
    }

    public void setCloseSound(Sound sound) {
        this.closeSound = sound;
    }

    public void setOpenInterval(int interval) {
        this.openInterval = interval;
    }

    public void setCloseInterval(int interval) {
        this.closeInterval = interval;
    }

    public int getOpenInterval() {
        return this.openInterval;
    }

    public int getCloseInterval() {
        return this.closeInterval;
    }
}
