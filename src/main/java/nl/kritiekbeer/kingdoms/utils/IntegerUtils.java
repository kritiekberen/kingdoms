package nl.kritiekbeer.kingdoms.utils;

import java.util.stream.IntStream;

public class IntegerUtils {

    public static int[] getIntsBetween(int left, int right) {
        int a = left;
        int b = right;
        if (left > right) {
            a = right;
            b = left;
        }

        int[] list;
        if (left == right) {
            list = new int[0];
        } else {
            list = IntStream.rangeClosed(a, b).toArray();
        }
        return list;
    }
}
