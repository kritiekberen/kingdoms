package nl.kritiekbeer.kingdoms.utils;

import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.Chunk;

public class ChunkUtils {

    public static boolean isConnected(Kingdom kingdom, Chunk chunk) {
        if (kingdom.getChunks().isEmpty()) return true;

        Chunk north = chunk.getWorld().getChunkAt(chunk.getX(), chunk.getZ() - 1);
        Chunk east = chunk.getWorld().getChunkAt(chunk.getX() + 1, chunk.getZ());
        Chunk south = chunk.getWorld().getChunkAt(chunk.getX(), chunk.getZ() + 1);
        Chunk west = chunk.getWorld().getChunkAt(chunk.getX() - 1, chunk.getZ());

        if (!kingdom.getChunks().contains(north) && !kingdom.getChunks().contains(east) &&
                !kingdom.getChunks().contains(south) && !kingdom.getChunks().contains(west))
            return false;

        return true;
    }
}
