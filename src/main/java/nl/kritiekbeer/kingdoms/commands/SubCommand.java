package nl.kritiekbeer.kingdoms.commands;

import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.List;

public abstract class SubCommand {

    public abstract String getIdentifier();
    public abstract List<String> getAliases();
    public abstract String getUsage();
    public abstract String getDescription();
    public abstract boolean execute(CPlayer player, String[] args);
    public abstract List<String> getAutoComplete(CPlayer player, String[] args);

}
