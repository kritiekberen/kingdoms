package nl.kritiekbeer.kingdoms.commands.gates;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.gates.subcommands.*;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.servercore.lang.messages.CommandNotFound;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GatesCommand implements CommandExecutor, TabCompleter {

    public static List<SubCommand> subCommands = Arrays.asList(new DeleteGateSubCommand(), new EditGateSubCommand(),
            new NewGateSubCommand(), new AddToggleSubCommand(), new RemoveToggleSubCommand(), new ListSubCommand());

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("You must be a player to execute this command");
            return true;
        }

        CPlayer player = Core.api.getPlayer(((Player)sender).getUniqueId());

        if (args.length == 0) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        if (Core.kingdomManager.getKingdomForPlayer(player) == null) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        String subcommand = args[0];

        for (SubCommand subCommand : subCommands) {
            if (subCommand.getAliases().contains(subcommand)) {
                return subCommand.execute(player, args);
            }
        }
        player.sendMessage(player.getLanguage().getMessage(new CommandNotFound()));
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (!(sender instanceof Player)) {
            return Arrays.asList("");
        }

        CPlayer player = Core.api.getPlayer(((Player)sender).getUniqueId());

        List<String> matches = new ArrayList<>();

        if (args.length == 1) {
            for (SubCommand subCommand : subCommands) {
                if (subCommand.getIdentifier().startsWith(args[0])) {
                    matches.add(subCommand.getIdentifier());
                }
            }
        }

        String subcommand = args[0];
        for (SubCommand subCommand : subCommands) {
            if (subCommand.getAliases().contains(subcommand)) {
                return subCommand.getAutoComplete(player, args);
            }
        }

        return matches;
    }
}
