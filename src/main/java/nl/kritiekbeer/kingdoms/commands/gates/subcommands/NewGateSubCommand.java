package nl.kritiekbeer.kingdoms.commands.gates.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.GateType;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.LocationUtils;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewGateSubCommand extends SubCommand {

    public String getIdentifier() {
        return "newgate";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "creategate", "create", "new");
    }

    public String getUsage() {
        return "<type> <name> <material>";
    }

    public String getDescription() {
        return "Create a new gate";
    }

    /*

    args[1] = GateType
    args[2] = Name
    args[3] = Material

     */
    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        if (args.length != 4) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        if (!kingdom.getRank(player).equals(RankType.KING) && !kingdom.getRank(player).equals(RankType.DUKE)) {
            player.sendMessage("You are not allowed to create a gate.");
            return true;
        }

        GateType gateType = GateType.valueOf(args[1].toUpperCase());
        String name = args[2].toLowerCase();
        Material material;
        try { material = Material.valueOf(args[3]); } catch (Exception e) { player.sendMessage("Not a valid material"); return true; }
        if (gateType == null) {
            player.sendMessage("Not a valid Gate Type");
            return true;
        }
        if (gateType != GateType.DRAWBRIDGE && !Core.materials.contains(material)) {
            player.sendMessage("Not a valid material");
            return true;
        }
        if (Core.gateManager.getGate(kingdom, name) != null) {
            player.sendMessage("You already have a gate with the name " + name);
            return true;
        }

        if (player.getVariable("leftbound").equals("") || player.getVariable("rightbound").equals("")) {
            player.sendMessage("Please make a gate selection first using a " + Core.gateTool.name());
            return true;
        }

        Location left = LocationUtils.locFromString((String)player.getVariable("leftbound"));
        Location right = LocationUtils.locFromString((String)player.getVariable("rightbound"));

        Chunk lChunk = left.getChunk();
        Chunk rChunk = right.getChunk();

        if (!kingdom.getChunks().contains(lChunk) && !kingdom.getChunks().contains(rChunk)) {
            player.sendMessage("You can only create gates in claimed land");
            return true;
        }

        Gate gate = Core.gateManager.createGate(kingdom, gateType, name, left, right, material);

        player.sendMessage("You have created a " + gate.getGateType().name().toLowerCase()
                + " gate with the name " + gate.getName());
        player.setVariable("leftbound","");
        player.setVariable("rightbound","");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        List<String> matches = new ArrayList<>();


        if (args.length == 2) {
            for (GateType gateType : GateType.values()) {
                if (gateType.name().toLowerCase().startsWith(args[1].toLowerCase())) {
                    matches.add(gateType.name());
                }
            }
        }
        else if (args.length == 4) {
            GateType setGateType = GateType.valueOf(args[1].toUpperCase());
            if (setGateType != null && setGateType == GateType.DRAWBRIDGE) {
                for (Material material : Material.values()) {
                    if (material.name().toLowerCase().startsWith(args[3].toLowerCase())) {
                        matches.add(material.name());
                    }
                }
            }else {
                for (Material material : Core.materials) {
                    if (material.name().toLowerCase().startsWith(args[3].toLowerCase())) {
                        matches.add(material.name());
                    }
                }
            }
        }

        return matches;
    }
}
