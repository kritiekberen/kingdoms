package nl.kritiekbeer.kingdoms.commands.gates.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveToggleSubCommand extends SubCommand {

    public String getIdentifier() {
        return "removetoggle";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "deltoggle");
    }

    public String getUsage() {
        return "<name>";
    }

    public String getDescription() {
        return "Remove the toggle from the gate";
    }

    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        String name = args[1].toLowerCase();
        Gate gate = Core.gateManager.getGate(kingdom, name);
        if (gate == null) {
            player.sendMessage("Gate with name " + name + " not found");
            return true;
        }

        if (!gate.isAllowedToEdit(player)) {
            player.sendMessage("You are not allowed to edit this gate.");
            return true;
        }

        Block block = player.getPlayer().getPlayer().getTargetBlockExact(10);
        if (block == null) {
            player.sendMessage("Could not find a block you are looking at");
            return true;
        }
        if (!gate.getTogglers().contains(block.getLocation())) {
            player.sendMessage("The target is not a valid toggle for gate " + gate.getName());
            return true;
        }

        gate.delToggler(block.getLocation());
        player.sendMessage("Removed toggle from gate " + gate.getName());

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        List<String> matches = new ArrayList<>();

        if (args.length == 2) {
            for (Gate gate : Core.gateManager.getGates(kingdom)) {
                if (gate.getName().toLowerCase().startsWith(args[1].toLowerCase()))
                    matches.add(gate.getName());
            }
        }

        return matches;
    }
}
