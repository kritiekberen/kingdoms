package nl.kritiekbeer.kingdoms.commands.gates.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.Methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListSubCommand extends SubCommand {

    public String getIdentifier() {
        return "list";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "gates", "listgates");
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "List all your gates";
    }

    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (Core.gateManager.getGates(kingdom).isEmpty()) {
            player.sendMessage("You don't have any gates");
            return true;
        }

        List<String> gates = new ArrayList<>();
        Core.gateManager.getGates(kingdom).forEach(gate -> gates.add(gate.getName()));
        player.sendMessage("Here is a list of your kingdoms' gates:");
        player.sendMessage(Methods.listToString(gates));

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return Arrays.asList("");
    }
}
