package nl.kritiekbeer.kingdoms.commands.gates.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeleteGateSubCommand extends SubCommand {

    public String getIdentifier() {
        return "deletegate";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "delgate", "removegate", "remove", "delete");
    }

    public String getUsage() {
        return "<name>";
    }

    public String getDescription() {
        return "Delete a gate";
    }

    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        String name = args[1].toLowerCase();

        Gate gate = Core.gateManager.getGate(kingdom, name);
        if (gate == null) {
            player.sendMessage("No gate found with the name " + name);
            return true;
        }
        if (!gate.isAllowedToEdit(player)) {
            player.sendMessage("You are not allowed to make changes to this gate.");
            return true;
        }

        Core.gateManager.removeGate(gate);
        player.sendMessage("Succesfully removed gate with the name " + name);

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        List<String> matches = new ArrayList<>();

        if (args.length == 2) {
            for (Gate gate : Core.gateManager.getGates(kingdom)) {
                if (gate.getName().startsWith(args[1].toLowerCase()))
                    matches.add(gate.getName());
             }
        }

        return matches;
    }
}
