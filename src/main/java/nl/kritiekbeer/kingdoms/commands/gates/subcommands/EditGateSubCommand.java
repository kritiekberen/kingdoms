package nl.kritiekbeer.kingdoms.commands.gates.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.EditType;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.lang.messages.PlayerNotFound;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EditGateSubCommand extends SubCommand {

    public String getIdentifier() {
        return "editgate";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "edit");
    }

    public String getUsage() {
        return "<name> <edittype> <value>";
    }

    public String getDescription() {
        return "Edit gates";
    }

    /*
     Edits
     - Name
     - Material type
     - addplayer
     - removeplayer
    */

    // editgate <name> <edittype> <newvalue>
    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 4) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        String name = args[1];
        Gate gate = Core.gateManager.getGate(kingdom, name);
        if (gate == null) {
            player.sendMessage("Gate with name " + name + " not found");
            return true;
        }

        if (!gate.isAllowedToEdit(player)) {
            player.sendMessage("You are not allowed to edit this gate.");
            return true;
        }


        EditType editType;
        try { editType = EditType.valueOf(args[2].toUpperCase());
        } catch (Exception e) { player.sendMessage("That edit type is not valid"); return true; }

        switch (editType) {
            case NAME: {
                gate.setName(args[3].toLowerCase());
                player.sendMessage("The new gate name is " + gate.getName());
                return true;
            }
            case MATERIAL: {
                Material material;
                try { material = Material.valueOf(args[3].toUpperCase()); } catch (Exception e) { player.sendMessage(args[3] + " is not a valid material"); return true; }

                if (!Core.materials.contains(material)) {
                    player.sendMessage(material.name() + " is not a valid gate material");
                    return true;
                }
                gate.setMaterial(material);
                player.sendMessage("The new gate material has been set to " + gate.getMaterial().name());
                return true;
            }
            case CLOSESOUND: {
                Sound sound;
                try {
                    sound = Sound.valueOf(args[3].toUpperCase());
                } catch (Exception e) {
                    player.sendMessage(args[3] + " is not a valid sound");
                    return true;
                }

                gate.setCloseSound(sound);
                player.sendMessage("The close sound has been set to " + gate.getCloseSound().name());
                return true;
            }
            case OPENSOUND: {
                Sound sound;
                try {
                    sound = Sound.valueOf(args[3].toUpperCase());
                } catch (Exception e) {
                    player.sendMessage(args[3] + " is not a valid sound");
                    return true;
                }

                gate.setOpenSound(sound);
                player.sendMessage("The open sound has been set to " + gate.getOpenSound().name());
                return true;
            }
            case CLOSEINTERVAL: {
                Integer interval;
                try { interval = Integer.valueOf(args[3]); } catch (Exception e) { player.sendMessage(args[3] + " is not a valid number"); return true; }

                gate.setCloseInterval(interval);
                player.sendMessage("The close interval has been set to " + gate.getCloseInterval());
                return true;
            }
            case OPENINTERVAL: {
                Integer interval;
                try { interval = Integer.valueOf(args[3]); } catch (Exception e) { player.sendMessage(args[3] + " is not a valid number"); return true; }

                gate.setOpenInterval(interval);
                player.sendMessage("The open interval has been set to " + gate.getOpenInterval());
                return true;
            }
        }

        player.sendMessage("§4error!");
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        List<String> matches = new ArrayList<>();
        EditType editType = null;
        try { editType = EditType.valueOf(args[2].toUpperCase());
        } catch (Exception e) {}

        if (args.length == 2) {
            for (Gate gate : Core.gateManager.getGates(kingdom)) {
                if (gate.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
                    matches.add(gate.getName());
                }
            }
        }
        else if (args.length == 3) {
            for (EditType edittype : EditType.values()) {
                if (edittype.name().toLowerCase().startsWith(args[2].toLowerCase())) {
                    matches.add(edittype.name());
                }
            }
        }
        else if (args.length == 4 && editType != null && editType == EditType.MATERIAL) {
            for (Material material : Core.materials) {
                if (material.name().toLowerCase().startsWith(args[3].toLowerCase())) {
                    matches.add(material.name().toUpperCase());
                }
            }
        }
        else if (args.length == 4 && editType != null && (editType == EditType.OPENSOUND || editType == EditType.CLOSESOUND)) {
            for (Sound sound : Sound.values()) {
                if (sound.name().toLowerCase().startsWith(args[3].toLowerCase())) {
                    matches.add(sound.name().toUpperCase());
                }
            }
        }

        return matches;
    }
}
