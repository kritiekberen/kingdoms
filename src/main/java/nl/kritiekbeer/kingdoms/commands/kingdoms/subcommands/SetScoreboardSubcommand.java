package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SetScoreboardSubcommand extends SubCommand {
    public String getIdentifier() {
        return "setscoreboard";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "<board>";
    }

    public String getDescription() {
        return "Set the scoreboard";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        BoardType type = BoardType.valueOf(args[1].toLowerCase());
        if (type == null) {
            player.sendMessage(args[1] + " is not a valid board");
            return true;
        }

        player.setVariable("boardtype", type.name());
        Core.boards.setBoard(player, type);
        player.sendMessage("Your board has been changed");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        List<String> matches = new ArrayList<>();

        for (BoardType type : BoardType.values()) {
            if (type.name().startsWith(args[1].toLowerCase()))
                matches.add(type.name());
        }

        return matches;
    }
}
