package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.kingdoms.kingdom.KingdomInvite;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InviteSubcommand extends SubCommand {
    public String getIdentifier() {
        return "invite";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "<player>";
    }

    public String getDescription() {
        return "Invite a player into your kingdom";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        CPlayer target = Core.api.getPlayer(args[1]);
        if (target == null) {
            player.sendMessage("Could not find a player with the name " + args[1]);
            return true;
        }

        if (Core.kingdomManager.getKingdomForPlayer(target) != null) {
            player.sendMessage(target.getName() + " is already in a kingdom");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        Core.invites.add(new KingdomInvite(kingdom, target, 500));

        player.sendMessage("You have send " + target.getName() + " an invite to join your kingdom.");
        target.sendMessage("You have been invited by the kingdom " + kingdom.getName() + ", invite expires in 500 seconds");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
