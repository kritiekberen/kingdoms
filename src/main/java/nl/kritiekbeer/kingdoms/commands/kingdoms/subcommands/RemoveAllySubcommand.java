package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveAllySubcommand extends SubCommand {
    public String getIdentifier() {
        return "removeally";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "delally");
    }

    @Override
    public String getUsage() {
        return "<kingdom>";
    }

    @Override
    public String getDescription() {
        return "Destory your alliance with the kingdom";
    }

    //kingdoms delally <kingdom>
    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        Kingdom target = Core.kingdomManager.getKingdom(args[1]);
        if (target == null) {
            player.sendMessage("Could not find a kingdom with the name " + args[1]);
            return true;
        }

        if (!kingdom.getAllies().contains(target)) {
            player.sendMessage("Your kingdom is not an ally with " + target.getName());
            return true;
        }

        kingdom.delAlly(target);
        target.delAlly(kingdom);

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
