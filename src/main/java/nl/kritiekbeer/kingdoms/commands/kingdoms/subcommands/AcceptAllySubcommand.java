package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AcceptAllySubcommand extends SubCommand {
    public String getIdentifier() {
        return "acceptally";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "<kingdom>";
    }

    public String getDescription() {
        return "Accept a kingdom ally request";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        Kingdom target = Core.kingdomManager.getKingdom(args[1]);
        if (target == null) {
            player.sendMessage("Could not find a kingdom with the name " + args[1]);
            return true;
        }

        if (!Core.getInstance().getAllyInvites(kingdom).contains(target)) {
            player.sendMessage("Your kingdom is not invited for alliance by " + target.getName() + ", or the invite has expired.");
            return true;
        }

        target.addAlly(kingdom);
        kingdom.addAlly(target);

        player.sendMessage("You have accepted " + target.getName() + "'s ally request");
        Core.getInstance().removeInvite(kingdom, target);
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
