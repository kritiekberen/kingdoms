package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.kingdoms.utils.ChunkUtils;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClaimSubcommand extends SubCommand {
    public String getIdentifier() {
        return "claim";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Claim curent chunk";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        Player p = player.getPlayer().getPlayer();

        if (Core.kingdomManager.isClaimed(p.getLocation().getChunk())) {
            player.sendMessage("That chunk is already claimed.");
            return true;
        }
        if (!p.getInventory().getItemInMainHand().getType().equals(Core.claimToken.getType())) {
            player.sendMessage("You don't have any claim tokens in your hand");
            return true;
        }
        if (!ChunkUtils.isConnected(kingdom, p.getLocation().getChunk())) {
            player.sendMessage("Your current chunk is not connected to your kingdom");
            return true;
        }

        ItemStack hand = p.getInventory().getItemInMainHand();
        hand.setAmount(hand.getAmount() - 1);
        p.getInventory().setItemInMainHand(hand);

        kingdom.addChunk(p.getLocation().getChunk());
        kingdom.sendMessage("A chunk has been claimed");
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
