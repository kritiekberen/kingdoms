package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateKingdomSubcommand extends SubCommand {
    public String getIdentifier() {
        return "createkingdom";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "newkingdom");
    }

    public String getUsage() {
        return "<name>";
    }

    @Override
    public String getDescription() {
        return "Create your own kingdom";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are already in a kingdom");
            return true;
        }

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        String name = args[1].toLowerCase();
        if (Core.kingdomManager.getKingdom(name) != null) {
            player.sendMessage("A kingdom with the name " + name.toLowerCase() + " already exists.");
            return true;
        }

        Core.kingdomManager.createKingdom(player, name);
        player.sendMessage("You have created a kingdom with the name " + name);

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
