package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.commands.kingdoms.KingdomCommand;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HelpSubcommand extends SubCommand {
    public String getIdentifier() {
        return "help";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "View help about all commands";
    }

    //kingdoms leave
    public boolean execute(CPlayer player, String[] args) {
        player.sendMessage("§7Kingdoms help: ");
        for (SubCommand subCommand : KingdomCommand.subCommands) {
            player.sendMessage("§b/kd " + subCommand.getIdentifier() + " " + subCommand.getUsage() + " §8- §7" + subCommand.getDescription());
        }

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
