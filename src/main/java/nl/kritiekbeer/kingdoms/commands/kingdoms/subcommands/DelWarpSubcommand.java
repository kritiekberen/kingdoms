package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DelWarpSubcommand extends SubCommand {
    public String getIdentifier() {
        return "deletewarp";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "delwarp", "removewarp");
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Remove the kingdom warp";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        kingdom.setWarp(null);
        player.sendMessage("Kingdom warp has been removed");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
