package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeclineInviteSubcommand extends SubCommand {
    public String getIdentifier() {
        return "declineinvite";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "<kingdom>";
    }

    public String getDescription() {
        return "Decline a kingdom invite";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are already in a kingdom");
            return true;
        }

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdom(args[1]);
        if (kingdom == null) {
            player.sendMessage("Could not find a kingdom with the name " + args[1]);
            return true;
        }

        if (!Core.getInstance().getInvites(player).contains(kingdom)) {
            player.sendMessage("You have not been invited by " + kingdom.getName() + ", or the invite has expired.");
            return true;
        }

        Core.getInstance().removeInvite(player, kingdom);
        kingdom.sendMessage(player.getName() + " has declined the kingdom invite");
        player.sendMessage("You have declined " + kingdom.getName() + "'s invite");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
