package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeleteKingdomSubcommand extends SubCommand {
    public String getIdentifier() {
        return "deletekingdom";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "removekingdom", "dispand");
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Delete your kingdom";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (Core.kingdomManager.getKingdom(player) == null) {
            player.sendMessage("You are not the owner of a kingdom");
            return true;
        }

        Core.kingdomManager.removeKingdom(player);
        player.sendMessage("You have deleted your kingdom");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
