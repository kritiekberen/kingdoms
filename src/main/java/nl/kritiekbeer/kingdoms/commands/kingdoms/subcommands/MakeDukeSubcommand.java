package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MakeDukeSubcommand extends SubCommand {
    public String getIdentifier() {
        return "makeduke";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "setduke");
    }

    public String getUsage() {
        return "<player>";
    }

    public String getDescription() {
        return "Make the player the duke of your kingdom";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        CPlayer target = Core.api.getPlayer(UUID.fromString(args[1]));
        if (target == null) {
            player.sendMessage("Could not find a player with the name " + args[1]);
            return true;
        }

        if (!kingdom.getMembers().contains(target)) {
            player.sendMessage(target.getName() + " is not a member of your kingdom");
            return true;
        }

        kingdom.setDuke(target);
        kingdom.sendMessage(target.getName() + " is the new duke");
        target.sendMessage("You are now the duke of " + kingdom.getName());
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
