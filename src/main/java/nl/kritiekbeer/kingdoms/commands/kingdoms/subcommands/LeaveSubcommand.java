package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LeaveSubcommand extends SubCommand {
    public String getIdentifier() {
        return "leave";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Leave your current kingdom";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        kingdom.kick(player);
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
