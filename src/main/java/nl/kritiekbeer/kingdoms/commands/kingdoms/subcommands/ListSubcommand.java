package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListSubcommand extends SubCommand {
    public String getIdentifier() {
        return "list";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "List all kingdoms";
    }

    public boolean execute(CPlayer player, String[] args) {
        String msg = "";
        if (Core.kingdomManager.getKingdoms().size() > 0) {
            for (Kingdom kingdom : Core.kingdomManager.getKingdoms()) {
                msg += ", " + kingdom.getName();
            }

            msg = msg.substring(2);
        } else {
            msg = "none";
        }

        player.sendMessage("All kingdoms: " + msg);

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
