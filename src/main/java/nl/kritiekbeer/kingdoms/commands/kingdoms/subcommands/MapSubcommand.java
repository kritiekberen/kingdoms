package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapSubcommand extends SubCommand {
    public String getIdentifier() {
        return "map";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "View chunks around you";
    }

    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        Player p = player.getPlayer().getPlayer();

        String[][] map = new String[9][9];

        int countx = 0;
        int countz = 0;

        for (int x = p.getLocation().getChunk().getX()-4; x < p.getLocation().getChunk().getX() + 5; x++) {
            for (int z = p.getLocation().getChunk().getZ()-4; z < p.getLocation().getChunk().getZ() + 5; z++) {
                String coord = ChatColor.WHITE + "/";

                Chunk chunk = p.getWorld().getChunkAt(x, z);

                if (kingdom == null && Core.kingdomManager.isClaimed(chunk)) {
                    coord = "X";
                }

                Kingdom claimed = Core.kingdomManager.getKingdom(chunk);
                if (claimed != null) {
                    if (claimed.equals(kingdom)) {
                        coord = ChatColor.GOLD + "X";
                    } else if (kingdom.getAllies().contains(claimed)) {
                        coord = ChatColor.GREEN + "X";
                    } else {
                        coord = ChatColor.WHITE + "X";
                    }
                }

                map[countx][countz] = coord;
                countz++;
            }
            countx++;
            countz = 0;
        }

        for (int x = 0; x < 9; x++) {
            String line = "";
            for (int z = 0; z < 9; z++) {
                line += map[x][z];
            }
            player.sendMessage(line);
        }

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
