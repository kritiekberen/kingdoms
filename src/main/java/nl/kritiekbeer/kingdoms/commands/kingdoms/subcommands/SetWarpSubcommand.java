package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SetWarpSubcommand extends SubCommand {
    public String getIdentifier() {
        return "setwarp";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "createwarp");
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Set the kingdom warp to your location";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (!kingdom.isClaimed(player.getPlayer().getPlayer().getLocation().getChunk())) {
            player.sendMessage("You can only set a warp in claimed chunks");
            return true;
        }

        kingdom.setWarp(player.getPlayer().getPlayer().getLocation());
        player.sendMessage("Kingdom warp has been set");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
