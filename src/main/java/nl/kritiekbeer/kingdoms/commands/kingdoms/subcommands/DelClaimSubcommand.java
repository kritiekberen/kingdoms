package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DelClaimSubcommand extends SubCommand {
    public String getIdentifier() {
        return "delclaim";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "removeclaim");
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Remove curent chunk";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);
        Player p = player.getPlayer().getPlayer();

        if (!kingdom.getChunks().contains(p.getLocation().getChunk())) {
            player.sendMessage("Your kingdom has not claimed this chunk");
            return true;
        }
        p.getInventory().addItem(Core.claimToken);
        kingdom.removeChunk(p.getLocation().getChunk());
        kingdom.sendMessage("A chunk has been disposed");
        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
