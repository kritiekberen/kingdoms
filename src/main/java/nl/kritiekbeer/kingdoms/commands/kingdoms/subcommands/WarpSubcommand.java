package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WarpSubcommand extends SubCommand {
    public String getIdentifier() {
        return "warp";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier());
    }

    public String getUsage() {
        return "";
    }

    public String getDescription() {
        return "Warp to your kingdom warp";
    }

    public boolean execute(CPlayer player, String[] args) {
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (kingdom.getWarp() == null) {
            player.sendMessage("Your kingdom doens't have a warp, please ask the king or duke to set it");
            return true;
        }

        player.teleport(kingdom.getWarp());
        player.sendMessage("You have been teleported to your kingdom warp");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
