package nl.kritiekbeer.kingdoms.commands.kingdoms.subcommands;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.commands.SubCommand;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.kingdoms.kingdom.KingdomAllyInvite;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddAllySubcommand extends SubCommand {
    public String getIdentifier() {
        return "addally";
    }

    public List<String> getAliases() {
        return Arrays.asList(getIdentifier(), "requestally");
    }

    public String getUsage() {
        return "<kingdom>";
    }

    public String getDescription() {
        return "Send a kingdom an ally request";
    }

    public boolean execute(CPlayer player, String[] args) {
        if (!Core.kingdomManager.inKingdom(player)) {
            player.sendMessage("You are not in a kingdom");
            return true;
        }

        if (Core.kingdomManager.getRank(player).getRankType() != RankType.KING &&
                Core.kingdomManager.getRank(player).getRankType() != RankType.DUKE) {
            player.sendMessage("You are not allowed to use this command");
            return true;
        }

        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (args.length != 2) {
            player.sendMessage(player.getLanguage().getMessage(new InsufficientArguments()));
            return true;
        }

        Kingdom target = Core.kingdomManager.getKingdom(args[1]);
        if (target == null) {
            player.sendMessage("Could not find a kingdom with the name " + args[1]);
            return true;
        }

        if (kingdom.getAllies().contains(target)) {
            player.sendMessage("Your kingdom is already an ally with " + target.getName());
            return true;
        }

        if (kingdom.getAllies().size() > (Core.kingdomManager.getKingdoms().size() / 5)) {
            player.sendMessage("You are currently not allowed to have any more allies");
            return true;
        }

        Core.allyInvites.add(new KingdomAllyInvite(kingdom, target, 500));
        player.sendMessage("You have requested an alliance with " + target.getName());
        target.sendLeaderMessage("You have recieved an alliance request by " + kingdom.getName() + ", you have 500 seconds to responc");

        return true;
    }

    public List<String> getAutoComplete(CPlayer player, String[] args) {
        return new ArrayList<>();
    }
}
