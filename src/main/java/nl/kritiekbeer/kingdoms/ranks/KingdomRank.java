package nl.kritiekbeer.kingdoms.ranks;

import nl.kritiekbeer.kingdoms.enums.RankType;
import org.bukkit.ChatColor;

public abstract class KingdomRank {

    public String name;
    private RankType rankType;
    private ChatColor color;

    public KingdomRank(String name, RankType rankType, ChatColor color) {
        this.name = name;
        this.rankType = rankType;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public RankType getRankType() {
        return rankType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatColor getColor() {
        return color;
    }

    public void setColor(ChatColor color) {
        this.color = color;
    }

    public String getPrefix() {
        return ChatColor.GRAY + "[" + color + name + ChatColor.GRAY + "]";
    }

    @Override
    public String toString() {
        return name + "," + color.name();
    }
}
