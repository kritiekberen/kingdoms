package nl.kritiekbeer.kingdoms.ranks;

import nl.kritiekbeer.kingdoms.enums.RankType;
import org.bukkit.ChatColor;

public class KingRank extends KingdomRank{
    public KingRank(String name) {
        super(name, RankType.KING, ChatColor.BLUE);
    }

    public KingRank(String name, ChatColor color) {
        super(name, RankType.KING, color);
    }
}
