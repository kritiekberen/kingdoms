package nl.kritiekbeer.kingdoms.ranks;

import nl.kritiekbeer.kingdoms.enums.RankType;
import org.bukkit.ChatColor;

public class DukeRank extends KingdomRank{
    public DukeRank(String name) {
        super(name, RankType.DUKE, ChatColor.BLUE);
    }

    public DukeRank(String name, ChatColor color) {
        super(name, RankType.KING, color);
    }
}
