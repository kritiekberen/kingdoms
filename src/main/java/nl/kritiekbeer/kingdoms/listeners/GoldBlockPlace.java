package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.LocationUtils;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class GoldBlockPlace implements Listener {

    @EventHandler
    public void onGoldBlockPlace(BlockPlaceEvent event) {
        if (!event.getBlock().getType().equals(Material.GOLD_BLOCK)) return;

        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (Core.kingdomManager.isClaimed(event.getBlock().getChunk())) {
            if (!Core.kingdomManager.getKingdom(event.getBlock().getChunk()).equals(kingdom)) {
                event.setCancelled(true);
                return;
            }

            else if (kingdom.getGoldBlock() == null) {
                kingdom.setGoldBlock(event.getBlock().getLocation());
                player.sendMessage("Gold block placed");
                return;
            }

            event.setCancelled(true);
            player.sendMessage("Your kingdom already has a Gold Block, please remove that one first. " + LocationUtils.locToString(kingdom.getGoldBlock()));

        }
    }
}
