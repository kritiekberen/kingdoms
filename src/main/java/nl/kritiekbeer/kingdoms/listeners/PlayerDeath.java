package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeath implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        CPlayer player = Core.api.getPlayer(event.getEntity().getUniqueId());
        int deaths = player.getVariable("deaths") == null ? 0 : (int)player.getVariable("deaths");
        deaths++;
        player.setVariable("deaths", deaths);

        if (event.getEntity().getKiller() != null) {
            CPlayer killer = Core.api.getPlayer(event.getEntity().getKiller().getUniqueId());
            int kills = killer.getVariable("kills") == null ? 0 : (int)killer.getVariable("kills");
            kills++;
            killer.setVariable("kills", kills);
        }
    }

}
