package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class GateBlockBreak implements Listener {

    @EventHandler
    public void onGateBreak(BlockBreakEvent event) {

        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());

        for (Gate gate : Core.gateManager.getGates()) {
            if (gate.isGateBlock(event.getBlock().getLocation())) {
                event.setCancelled(true);
                player.sendMessage("You are not allowed to break that block, since it's a gate");
                return;
            }
        }
    }
}
