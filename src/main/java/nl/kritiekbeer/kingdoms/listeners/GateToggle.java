package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.gates.Gate;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class GateToggle implements Listener {

    @EventHandler
    public void onToggle(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        Material material = event.getClickedBlock().getType();
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());

        if (!Core.togglers.contains(material)) {
            return;
        }

        for (Gate gate : Core.gateManager.getGatesByToggle(event.getClickedBlock().getLocation())) {

            if (gate.isClosing()) {
                player.sendMessage("Gate " + gate.getName() + " is already in progress");
                return;
            }

            if (gate.isAllowed(player) || gate.getOwner().equals(player)) {
                if (gate.isClosed()) {
                    gate.open();
                } else {
                    gate.close();
                }
            } else {
                player.sendMessage("You are not allowed to control the gate " + gate.getName());
                event.setCancelled(true);
            }
        }
        return;
    }
}
