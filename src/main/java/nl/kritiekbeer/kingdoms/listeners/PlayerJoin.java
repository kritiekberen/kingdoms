package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.BoardType;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());
        BoardType type;
        if (player.getVariable("boardtype") == null) {
            type = BoardType.kingdom;
            player.setVariable("boardtype", BoardType.kingdom.name());
        }
        else
            type = BoardType.valueOf((String)player.getVariable("boardtype"));

        Core.boards.setBoard(player, type);
    }
}
