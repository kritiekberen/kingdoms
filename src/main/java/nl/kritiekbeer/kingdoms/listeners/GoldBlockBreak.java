package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GoldBlockBreakEvent;
import nl.kritiekbeer.kingdoms.defendthecastle.game.Game;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GoldBlockBreak implements Listener {

    @EventHandler
    public void onGoldBlockBreak(GoldBlockBreakEvent event) {
        Kingdom ownKingdom = Core.kingdomManager.getKingdomForPlayer(event.getPlayer());

        if (!event.getKingdom().isDefending()) {
            if (event.getKingdom().equals(ownKingdom)) {
                ownKingdom.setGoldBlock(null);
                event.getPlayer().sendMessage("Gold block broken");
                return;
            }

            event.getPlayer().sendMessage("You are not allowed to break blocks in the kingdom: " + event.getKingdom().getName());
            return;
        }

        Game game = Core.gameManager.getGame(event.getKingdom());
        game.setWinner(ownKingdom);
        game.stop(false);
    }
}
