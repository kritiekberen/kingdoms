package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.LocationUtils;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class GateInteract implements Listener {

        @EventHandler
        public void onInteract(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();

        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());

        if (event.getMaterial() == null || !event.getMaterial().equals(Core.gateTool)) return;

        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            event.setCancelled(true);
            player.setVariable("leftbound", LocationUtils.locToString(block.getLocation()));
            player.sendMessage("Leftbound: " + LocationUtils.locToString(block.getLocation()));
        }
        else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            event.setCancelled(true);
            player.setVariable("rightbound", LocationUtils.locToString(block.getLocation()));
            player.sendMessage("Rightbound: " + LocationUtils.locToString(block.getLocation()));
        }
    }
}
