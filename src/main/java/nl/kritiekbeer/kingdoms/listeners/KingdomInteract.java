package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GoldBlockBreakEvent;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class KingdomInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_AIR))
            return;
        if (!Core.kingdomManager.isClaimed(event.getClickedBlock().getChunk()))
            return;

        Kingdom kingdom = Core.kingdomManager.getKingdom(event.getClickedBlock().getChunk());
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());
        if (!kingdom.getMembers().contains(player) && !player.getPlayer().getPlayer().hasPermission("kingdoms.place.all")) {
            event.setCancelled(true);

            if (event.getClickedBlock().getType().equals(Material.GOLD_BLOCK)) {
                Bukkit.getServer().getPluginManager().callEvent(new GoldBlockBreakEvent(kingdom, player));
                return;
            }

            player.sendMessage("You are not allowed to interact with blocks in the kingdom: " + kingdom.getName());
        }
    }
}
