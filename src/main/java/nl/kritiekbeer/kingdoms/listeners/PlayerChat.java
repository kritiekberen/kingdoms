package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());
        Kingdom kingdom = Core.kingdomManager.getKingdomForPlayer(player);

        if (kingdom != null) {
            event.setCancelled(true);
            Bukkit.broadcastMessage("["+kingdom.getName()+"] <" + player.getName() + "> " + event.getMessage());
        }
    }
}
