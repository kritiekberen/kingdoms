package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GoldBlockBreakEvent;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class KingdomBlockPlace implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!Core.kingdomManager.isClaimed(event.getBlock().getChunk()))
            return;

        Kingdom kingdom = Core.kingdomManager.getKingdom(event.getBlock().getChunk());
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());
        if (!kingdom.getMembers().contains(player) && !player.getPlayer().getPlayer().hasPermission("kingdoms.place.all")) {
            event.setCancelled(true);

            if (event.getBlock().getType().equals(Material.GOLD_BLOCK)) {
                Bukkit.getServer().getPluginManager().callEvent(new GoldBlockBreakEvent(kingdom, player));
                return;
            }

            player.sendMessage("You are not allowed to place blocks in the kingdom: " + kingdom.getName());
        }
    }
}
