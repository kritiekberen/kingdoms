package nl.kritiekbeer.kingdoms.listeners;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GoldBlockBreakEvent;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class KingdomBlockBreak implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (!Core.kingdomManager.isClaimed(event.getBlock().getChunk()))
            return;

        Kingdom kingdom = Core.kingdomManager.getKingdom(event.getBlock().getChunk());
        CPlayer player = Core.api.getPlayer(event.getPlayer().getUniqueId());

        if (event.getBlock().getType().equals(Material.GOLD_BLOCK)) {
            Bukkit.getServer().getPluginManager().callEvent(new GoldBlockBreakEvent(kingdom, player));
            return;
        }

        if (!kingdom.getMembers().contains(player) && !player.getPlayer().getPlayer().hasPermission("kingdoms.break.all")) {
            event.setCancelled(true);
            player.sendMessage("You are not allowed to break blocks in the kingdom: " + kingdom.getName());
        }
    }
}
