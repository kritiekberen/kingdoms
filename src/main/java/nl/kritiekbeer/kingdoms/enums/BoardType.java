package nl.kritiekbeer.kingdoms.enums;

public enum BoardType {
    kingdom,
    stats,
    map,
    none
}