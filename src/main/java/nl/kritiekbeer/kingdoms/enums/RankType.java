package nl.kritiekbeer.kingdoms.enums;

public enum RankType {
    KING,
    DUKE,
    PEASANT
}
