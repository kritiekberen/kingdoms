package nl.kritiekbeer.kingdoms.enums;

public enum GateType {
    SLIDING,
    FENCE,
    DRAWBRIDGE
}
