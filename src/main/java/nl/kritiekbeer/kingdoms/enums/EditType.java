package nl.kritiekbeer.kingdoms.enums;

public enum EditType {
    NAME,
    MATERIAL,
    OPENSOUND,
    CLOSESOUND,
    OPENINTERVAL,
    CLOSEINTERVAL
}
