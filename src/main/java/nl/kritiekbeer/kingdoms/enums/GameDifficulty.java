package nl.kritiekbeer.kingdoms.enums;

public enum GameDifficulty {
    EASY(10000),
    NORMAL(20000),
    HARD(30000);

    private int time;
    GameDifficulty(int time) {
        this.time = time;
    }

    public int getDuration() {
        return time;
    }
}
