package nl.kritiekbeer.kingdoms.defendthecastle.game;

import nl.kritiekbeer.kingdoms.enums.GameDifficulty;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class GameLoot {

    private List<ItemStack> loot;

    public GameLoot(GameDifficulty gameDifficulty) {
        loot = new ArrayList<>();

        switch (gameDifficulty) {
            case EASY: {
                //TODO ADD LOOT
            }
            case NORMAL: {

            }
            case HARD: {

            }
        }
    }

    public List<ItemStack> getLoot() {
        return loot;
    }

}
