package nl.kritiekbeer.kingdoms.defendthecastle.game;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GameFinishEvent;
import nl.kritiekbeer.kingdoms.defendthecastle.events.GameStartEvent;
import nl.kritiekbeer.kingdoms.enums.GameDifficulty;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;

public class Game extends BukkitRunnable {

    private int id;
    private Kingdom defending;
    private Kingdom winner = null;
    private GameDifficulty gameDifficulty;
    private int timeLeft = 0;
    private boolean running;

    public Game(Kingdom defending, GameDifficulty gameDifficulty) {
        this.id = Core.gameManager.getGames().size();
        this.defending = defending;
        this.gameDifficulty = gameDifficulty;
        this.timeLeft = gameDifficulty.getDuration();
    }

    public Kingdom getDefending() {
        return defending;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public int getID () {
        return this.id;
    }

    public void setWinner(Kingdom winner) {
        this.winner = winner;
    }

    public void start() {
        Bukkit.getServer().getPluginManager().callEvent(new GameStartEvent(defending));
        running = true;
    }

    public void stop(boolean won) {
        Bukkit.getServer().getPluginManager().callEvent(new GameFinishEvent(defending, won));
        GameLoot loot = new GameLoot(gameDifficulty);

        if (won) winner = defending;

        winner.getOnlineMembers().forEach(member -> {
            loot.getLoot().forEach(item -> member.getPlayer().getPlayer().getInventory().addItem(item));
        });

        running = false;
    }

    public boolean running() {
        return running;
    }

    public void run() {
        this.timeLeft--;
        if (this.timeLeft == 0)
            stop(true);


        List<Integer> marks = Arrays.asList(20000, 10000, 5000, 2000, 1000, 500);
        if (marks.contains(timeLeft)) {
            Bukkit.getServer().broadcastMessage("The attack on " + defending.getName() + " ends in " + (timeLeft/20) + " minutes");
        }


    }
}
