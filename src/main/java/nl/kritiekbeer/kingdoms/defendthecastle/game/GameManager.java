package nl.kritiekbeer.kingdoms.defendthecastle.game;

import nl.kritiekbeer.kingdoms.enums.GameDifficulty;
import nl.kritiekbeer.kingdoms.kingdom.Kingdom;

import java.util.ArrayList;
import java.util.List;

public class GameManager {

    private List<Game> games;

    public GameManager() {
        this.games = new ArrayList<>();
    }

    public void createGame(Kingdom defending, GameDifficulty gameDifficulty) {
        this.games.add(new Game(defending, gameDifficulty));
    }

    public void stopAll() {
        this.games.forEach(game -> game.stop(false));
    }

    public Game getGame(int id) {
        for (Game game : this.games) {
            if (game.getID() == id)
                return game;
        }
        return null;
    }

    public Game getGame(Kingdom defending) {
        for (Game game : this.games) {
            if (game.getDefending().equals(defending))
                return game;
        }
        return null;
    }

    public List<Game> getGames() {
        return this.games;
    }

    public void run() {
        this.games.forEach(game -> {
            if (game.running()) game.run();
        });
    }
}
