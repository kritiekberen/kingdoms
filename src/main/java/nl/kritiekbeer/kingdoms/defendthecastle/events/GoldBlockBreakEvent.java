package nl.kritiekbeer.kingdoms.defendthecastle.events;

import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GoldBlockBreakEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private Kingdom kingdom;
    private CPlayer player;

    public GoldBlockBreakEvent(Kingdom kingdom, CPlayer player) {
        this.kingdom = kingdom;
        this.player = player;
    }

    public Kingdom getKingdom() {
        return kingdom;
    }

    public CPlayer getPlayer() {
        return player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}