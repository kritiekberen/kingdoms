package nl.kritiekbeer.kingdoms.defendthecastle.events;

import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameFinishEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private Kingdom defending;
    private Boolean won;

    public GameFinishEvent(Kingdom defending, boolean won) {
        this.defending = defending;
        this.won = won;
    }

    public Kingdom getDefending() {
        return defending;
    }

    public Boolean defendersWon() {
        return won;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
