package nl.kritiekbeer.kingdoms.defendthecastle.events;

import nl.kritiekbeer.kingdoms.kingdom.Kingdom;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStartEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private Kingdom defending;

    public GameStartEvent(Kingdom defending) {
       this.defending = defending;
    }

    public Kingdom getDefending() {
        return defending;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
