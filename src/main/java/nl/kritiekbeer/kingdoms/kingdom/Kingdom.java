package nl.kritiekbeer.kingdoms.kingdom;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.ranks.DukeRank;
import nl.kritiekbeer.kingdoms.ranks.KingRank;
import nl.kritiekbeer.kingdoms.ranks.KingdomRank;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.LocationUtils;
import org.bukkit.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Kingdom {

    private int id;
    private CPlayer owner, duke;
    private List<CPlayer> members;
    private String name;
    private List<Kingdom> allies;
    private Location kingdomBlock, warp;
    private List<Chunk> chunks;
    private KingdomRank kingRank, dukeRank;

    public Kingdom(int id, CPlayer owner, String name) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.members = new ArrayList<>();
        this.members.add(owner);
        this.duke = null;
        this.allies = new ArrayList<>();
        this.kingdomBlock = null;
        this.chunks = new ArrayList<>();
        this.kingRank = new KingRank("KING");
        this.dukeRank = new DukeRank("DUKE");
        this.owner.setVariable("kingdomrank", RankType.KING.name());
        this.warp = null;
    }

    public Kingdom(int id) {
        this.id = id;
    }

    public CPlayer getOwner() {
        return this.owner;
    }

    public String getName() {
        return this.name;
    }

    public List<Chunk> getChunks() {
        return this.chunks;
    }

    public int getID() {
        return this.id;
    }

    public List<CPlayer> getMembers() {
        return this.members;
    }

    public List<CPlayer> getOnlineMembers() {
        List<CPlayer> list = new ArrayList<>();
        for (CPlayer member : this.members) {
            if (member.isOnline()) list.add(member);
        } return list;
    }

    public void addPeasant(CPlayer player) {
        if (!this.members.contains(player))
            this.members.add(player);
        player.setVariable("kingdomrank", RankType.PEASANT);
    }

    public void sendMessage(String message) {
        this.getMembers().forEach(member -> {
            if (member.isOnline()) member.sendMessage(message);
        });
    }

    public void sendLeaderMessage(String message) {
        this.getMembers().forEach(member -> {
            if (member.isOnline()) {
                if (getRank(member).equals(RankType.KING) ||
                        getRank(member).equals(RankType.DUKE))
                    member.sendMessage(message);
            }
        });
    }

    public void kick(CPlayer target) {
        if (this.getMembers().contains(target)) {
            if (target.getVariable("kingdomrank").equals(RankType.DUKE.name())) {
                this.duke = null;
            }

            this.members.remove(target);
            target.setVariable("kingdomrank", "");
            target.sendMessage("You have been kicked from your kingdom");
        }
    }

    public void addAlly(Kingdom kingdom) {
        if (!this.allies.contains(kingdom)) {
            this.allies.add(kingdom);
            this.sendMessage(kingdom.getName() + " is now an ally");
        }
    }

    public void delAlly(Kingdom kingdom) {
        if (this.allies.contains(kingdom)) {
            this.allies.remove(kingdom);
            this.sendMessage(kingdom.getName() + " is no longer an ally");
        }
    }

    public void setDuke(CPlayer player) {
        CPlayer old = this.duke;
        this.duke = player;
        old.sendMessage("You are no longer the duke of " + getName());
    }

    public void delDuke() {
        this.duke.sendMessage("You are no longer the duke of " + getName());
        this.duke = null;
    }

    public List<Kingdom> getAllies() {
        return this.allies;
    }

    public KingdomRank getRank(RankType rankType) {
        if (rankType == RankType.KING)
            return this.kingRank;
        if (rankType == RankType.DUKE)
            return this.dukeRank;
        return null;
    }

    public RankType getRank(CPlayer player) {
        return getRank(RankType.valueOf((String)player.getVariable("kingdomrank"))).getRankType();
    }

    public Location getWarp() {
        return warp;
    }

    public void setWarp(Location warp) {
        this.warp = warp;
    }

    public void addChunk(Chunk chunk) {
        if (!this.chunks.contains(chunk)) this.chunks.add(chunk);
    }

    public boolean isDefending() {
        return (Core.gameManager.getGame(this) != null);
    }

    public void removeChunk(Chunk chunk) {
        if (this.chunks.contains(chunk)) this.chunks.remove(chunk);
    }

    public void setGoldBlock(Location location) {
        this.kingdomBlock = location;
    }

    public Location getGoldBlock() {
        return this.kingdomBlock;
    }

    public void delete() {
        this.members.forEach(member -> member.setVariable("kingdomrank", ""));
    }

    public boolean isClaimed(Chunk chunk) {
        for (Chunk claimed : getChunks()) {
            if (claimed.getX() == chunk.getX() && claimed.getZ() == chunk.getZ() && claimed.getWorld().getName().equals(chunk.getWorld().getName())) {
                return true;
            }
        } return false;
    }

    public void save() {
        List<String> memberList = new ArrayList<>();
        members.forEach(member -> memberList.add(member.getUniqueId().toString()));

        List<String> chunkList = new ArrayList<>();
        chunks.forEach(chunk -> chunkList.add(chunk.getWorld().getName() + "," + chunk.getX() + "," + chunk.getZ()));

        List<Integer> allyList = new ArrayList<>();
        allies.forEach(ally -> allyList.add(ally.getID()));

        Core.kingdoms.set(id + ".name", name);
        Core.kingdoms.set(id + ".king", owner.getUniqueId().toString());
        Core.kingdoms.set(id + ".duke", duke == null ? "" : duke.getUniqueId().toString());
        Core.kingdoms.set(id + ".kingdomblock", kingdomBlock == null ? "" : LocationUtils.locToString(kingdomBlock));
        Core.kingdoms.set(id + ".members", memberList);
        Core.kingdoms.set(id + ".chunks", chunkList);
        Core.kingdoms.set(id + ".allies", allyList);
        Core.kingdoms.set(id + ".kingRank", kingRank.toString());
        Core.kingdoms.set(id + ".dukeRank", dukeRank.toString());
        Core.kingdoms.set(id + ".warp", warp == null ? "" : LocationUtils.locToString(warp));
    }

    public void load() {
        members = new ArrayList<>();
        chunks = new ArrayList<>();
        allies = new ArrayList<>();

        this.owner = Core.api.getPlayer(UUID.fromString(Core.kingdoms.getString(id + ".king")));
        this.duke = Core.kingdoms.getString(id + ".duke").equals("") ? null : Core.api.getPlayer(UUID.fromString(Core.kingdoms.getString(id + ".duke")));
        this.kingdomBlock = Core.kingdoms.getString(id + ".kingdomblock").isEmpty() ? null : LocationUtils.locFromString(Core.kingdoms.getString(id + ".kingdomblock", null));
        this.name = Core.kingdoms.getString(id + ".name");
        this.warp = Core.kingdoms.getString(id + ".warp").isEmpty() ? null : LocationUtils.locFromString(Core.kingdoms.getString(id + ".warp"));

        Core.kingdoms.getStringList(id + ".members").forEach(uuid -> members.add(Core.api.getPlayer(UUID.fromString(uuid))));
        Core.kingdoms.getStringList(id + ".chunks").forEach(location -> {
            String[] data = location.split(",");
            World world = Bukkit.getWorld(data[0]);
            chunks.add(world.getChunkAt(Integer.valueOf(data[1]), Integer.valueOf(data[2])));
        });
        Core.kingdoms.getIntegerList(id + ".allies").forEach(allyId -> allies.add(Core.kingdomManager.getKingdom(allyId)));

        String[] kingRankData = Core.kingdoms.getString(id + ".kingRank").split(",");
        String[] dukeRankData = Core.kingdoms.getString(id + ".dukeRank").split(",");

        this.kingRank = new KingRank(kingRankData[0], ChatColor.valueOf(kingRankData[1]));
        this.dukeRank = new DukeRank(dukeRankData[0], ChatColor.valueOf(dukeRankData[1]));
    }
}

