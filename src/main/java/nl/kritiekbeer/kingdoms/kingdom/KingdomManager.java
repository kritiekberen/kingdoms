package nl.kritiekbeer.kingdoms.kingdom;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.kingdoms.enums.RankType;
import nl.kritiekbeer.kingdoms.ranks.KingdomRank;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;

import java.util.ArrayList;
import java.util.List;

public class KingdomManager {

    private List<Kingdom> kingdoms;

    public KingdomManager() {
        this.kingdoms = new ArrayList<>();
        this.load();
    }

    /**
     * Create a kingdom
     *
     * @param owner Owner of the kingdom
     * @param name Name of the kingdom
     * @return Created kingdom
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom createKingdom(CPlayer owner, String name) {
        Kingdom kingdom = new Kingdom(this.kingdoms.size(), owner, name);
        this.kingdoms.add(kingdom);
        return kingdom;
    }

    /**
     * Remove a kingdom
     *
     * @param owner Owner of the kingdom to remove
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void removeKingdom(CPlayer owner) {
        if (getKingdom(owner) != null) {
            Kingdom kingdom = getKingdom(owner);
            kingdom.delete();
            this.kingdoms.remove(kingdom);
        }
    }

    /**
     * Find a kingdom by it's name
     *
     * @param name Name to search by
     * @return Found kingdom or null
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom getKingdom(String name) {
        for (Kingdom kingdom : this.kingdoms){
            if (kingdom.getName().toLowerCase().equalsIgnoreCase(name.toLowerCase()))
                return kingdom;
        }
        return null;
    }

    /**
     * Find a kingdom by it's owner
     *
     * @param owner Owner to search by
     * @return Found kingdom or null
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom getKingdom(CPlayer owner) {
        for (Kingdom kingdom : this.kingdoms){
            if (kingdom.getOwner().equals(owner))
                return kingdom;
        }
        return null;
    }

    /**
     * Find a kingdom by it's id
     *
     * @param id Id to search by
     * @return Found kingdom or null
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom getKingdom(int id) {
        for (Kingdom kingdom : this.kingdoms){
            if (kingdom.getID() == id)
                return kingdom;
        }
        return null;
    }

    /**
     * Find kingdom by claimed chunk
     *
     * @param chunk Chunk to check by
     * @return Found kingdom or null
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom getKingdom(Chunk chunk) {
        for (Kingdom kingdom : kingdoms) {
            for (Chunk claimed : kingdom.getChunks()) {
                if (claimed.getX() == chunk.getX() && claimed.getZ() == chunk.getZ() && claimed.getWorld().getName().equals(chunk.getWorld().getName()))
                    return kingdom;
            }
        } return null;
    }

    /**
     * Get all kingdoms
     *
     * @return Return a list of all kingdoms
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public List<Kingdom> getKingdoms() {
        return kingdoms;
    }

    /**
     * Return all claimed chunks
     * @return List of Chunk
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public List<Chunk> getClaimedChunks() {
        List<Chunk> chunks = new ArrayList<>();
        for (Kingdom kingdom : kingdoms) {
            for (Chunk chunk : kingdom.getChunks()) {
                chunks.add(chunk);
            }
        }

        return chunks;
    }

    /**
     * Check if a chunk is claimed by any kingdom
     *
     * @param chunk Chunk to check
     * @return True if claimed
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public boolean isClaimed(Chunk chunk) {
        for (Chunk claimed : getClaimedChunks()) {
            if (claimed.getX() == chunk.getX() && claimed.getZ() == chunk.getZ() && claimed.getWorld().getName().equals(chunk.getWorld().getName())) {
                return true;
            }
        } return false;
    }

    /**
     * Load all kingdoms data
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void load() {
        if (Core.kingdoms.isConfigurationSection("")) {
            for (String id : Core.kingdoms.getConfigurationSection("").getKeys(false)) {
                Kingdom kingdom = new Kingdom(Integer.valueOf(id));
                this.kingdoms.add(kingdom);
                kingdom.load();
            }
        }
        Bukkit.getConsoleSender().sendMessage("[Kingdoms] Loaded " + this.kingdoms.size() + " kingdoms.");
        Bukkit.getConsoleSender().sendMessage("[Kingdoms] All claimed chunks: " + getClaimedChunks().toString());
    }

    /**
     * Save all kingdoms data
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void save() {
        this.kingdoms.forEach(Kingdom::save);
    }

    /**
     * Find the kingdom the player is in
     *
     * @param player Player to check for
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Kingdom getKingdomForPlayer(CPlayer player) {
        for (Kingdom kingdom : this.kingdoms) {
            if (kingdom.getMembers().contains(player))
                return kingdom;
        }
        return null;
    }

    /**
     * Get player rank
     *
     * @param player Player to get rank for
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public KingdomRank getRank(CPlayer player) {
        Kingdom kingdom = getKingdomForPlayer(player);
        if (kingdom != null) {
            return kingdom.getRank(RankType.valueOf((String)player.getVariable("kingdomrank")));
        }
        return null;
    }

    /**
     * Test if the player is in a kingdom
     *
     * @param player Player to test
     * @return Boolean if player is in a kingdom
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public boolean inKingdom(CPlayer player) {
        return getKingdomForPlayer(player) != null;
    }

}
