package nl.kritiekbeer.kingdoms.kingdom;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.servercore.player.CPlayer;

public class KingdomAllyInvite {

    private Kingdom kingdom, target;
    private int timeout;

    public KingdomAllyInvite(Kingdom kingdom, Kingdom target, int timeout) {
        this.kingdom = kingdom;
        this.target = target;
        this.timeout = timeout;
    }

    public Kingdom getTarget() {
        return target;
    }

    public Kingdom getKingdom() {
        return kingdom;
    }

    public void count() {
        this.timeout--;
        if (this.timeout == 0) {
            Core.invites.remove(this);
        }
    }
}
