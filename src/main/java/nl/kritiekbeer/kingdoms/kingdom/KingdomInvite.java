package nl.kritiekbeer.kingdoms.kingdom;

import nl.kritiekbeer.kingdoms.Core;
import nl.kritiekbeer.servercore.player.CPlayer;

public class KingdomInvite {

    private Kingdom kingdom;
    private CPlayer target;
    private int timeout;

    public KingdomInvite(Kingdom kingdom, CPlayer target, int timeout) {
        this.kingdom = kingdom;
        this.target = target;
        this.timeout = timeout;
    }

    public CPlayer getTarget() {
        return target;
    }

    public Kingdom getKingdom() {
        return kingdom;
    }

    public void count() {
        this.timeout--;
        if (this.timeout == 0) {
            Core.invites.remove(this);
        }
    }
}
